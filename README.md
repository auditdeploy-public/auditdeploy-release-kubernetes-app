# Overview

Release is a deployment orchestration platform. This installation includes the Release UI, the Deployer microservice for deploying applications with Release, and an ArangoDB database.

For more information on Release, visit [AuditDeploy](https://www.auditdeploy.com/products/release).

# Installation

## Quick install with Google Cloud Marketplace

Get up and running with a few clicks! Install Release to a Google
Kubernetes Engine cluster using Google Cloud Marketplace. Follow the
[on-screen instructions](REPLACE_ME_WITH_MARKETPLACE_URL).

## Command line instructions

You can use [Google Cloud Shell](https://cloud.google.com/shell/) or a local
workstation to complete these steps.

### Prerequisites

#### Set up command-line tools

You'll need the following tools in your development environment. If you are using Cloud Shell, these tools are installed in your environment.

- [gcloud](https://cloud.google.com/sdk/gcloud/)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [docker](https://docs.docker.com/install/)
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [helm](https://helm.sh/)

Configure `gcloud` as a Docker credential helper:

```shell
gcloud auth configure-docker
```

#### Create a Google Kubernetes Engine (GKE) cluster

Create a new cluster from the command line:

```shell
export CLUSTER=release-cluster
export ZONE=us-central1-a

gcloud container clusters create "$CLUSTER" --zone "$ZONE"
```

Configure `kubectl` to connect to the new cluster.

```shell
gcloud container clusters get-credentials "$CLUSTER" --zone "$ZONE"
```

#### Clone this repo

Clone this repo and the associated tools repo:

```shell
git clone --recursive https://gitlab.com/auditdeploy-team/auditdeploy-release-kubernetes-app.git # REPLACE ME WITH PUBLIC REPO
```

#### Install the Application resource definition

An Application resource is a collection of individual Kubernetes components,
such as Services, Deployments, and so on, that you can manage as a group.

To set up your cluster to understand Application resources, run the following command:

```shell
kubectl apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"
```

You need to run this command once.

The Application resource is defined by the
[Kubernetes SIG-apps](https://github.com/kubernetes/community/tree/master/sig-apps) community. The source code can be found on
[github.com/kubernetes-sigs/application](https://github.com/kubernetes-sigs/application).

### Install Release

#### Configure the environment variables

Choose an instance name and
[namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
for the app. In most cases, you can use the `default` namespace.

```shell
export APP_INSTANCE_NAME=release
export NAMESPACE=default
```

Set up the image tag:

It is advised to use stable image reference which you can find on
[Marketplace Container Registry](REPLACE_ME_WITH_MARKETPLACE_CONTAINER_REGISTRY).
Example:

```shell
export TAG="0.1.0"
```

Configure the container image:

```shell
export IMAGE_RELEASE="REPLACE ME WITH RELEASE GCR IMAGE"
export IMAGE_ARANGODB="REPLACE ME WITH ARANGODB GCR IMAGE"
export IMAGE_DEPLOYER="REPLACE ME WITH DEPLOYER GCR IMAGE"
```

For the persistent disk provisioning for ArangoDB, you will need to:

 * Set the StorageClass name. Check your available options using the command below:
   * ```kubectl get storageclass```
   * Or check how to create a new StorageClass in [Kubernetes Documentation](https://kubernetes.io/docs/concepts/storage/storage-classes/#the-storageclass-resource)

 * Set the persistent disk's size. The default disk size is "30Gi".

```shell
export ARANGODB_STORAGE_CLASS="standard" # provide your StorageClass name if not "standard"
export ARANGODB_PERSISTENT_DISK_SIZE="30Gi"
```

Configure the ArangoDB user's credentials (passwords must be encoded in base64):

```shell
export ARANGODB_ROOT_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1 | tr -d '\n' | base64)
```

#### Create namespace in your Kubernetes cluster

If you use a different namespace than the `default`, run the command below to create a new namespace:

```shell
kubectl create namespace "$NAMESPACE"
```

#### Expand the manifest template

Use `helm template` to expand the template. We recommend that you save the
expanded manifest file for future updates to the application.

```shell
helm template "$APP_INSTANCE_NAME" chart/auditdeploy-release \
  --namespace "$NAMESPACE" \
  --set release.image.repository="$IMAGE_RELEASE" \
  --set arangodb.image.repository="$IMAGE_ARANGODB" \
  --set deployer.image.repository="$IMAGE_DEPLOYER" \
  --set arangodb.password="$ARANGODB_ROOT_PASSWORD" \
  > "${APP_INSTANCE_NAME}_manifest.yaml"
```

#### Apply the manifest to your Kubernetes cluster

Use `kubectl` to apply the manifest to your Kubernetes cluster:

```shell
kubectl apply -f "${APP_INSTANCE_NAME}_manifest.yaml" --namespace "${NAMESPACE}"
```

#### View the app in the Google Cloud Platform Console

To get the Console URL for your app, run the following command:

```shell
echo "https://console.cloud.google.com/kubernetes/application/${ZONE}/${CLUSTER}/${NAMESPACE}/${APP_INSTANCE_NAME}"
```

To view the app, open the URL in your browser.

### Access Release within the network

As an alternative to exposing Release publicly, you can use local port
forwarding. In a background terminal, run the following command:

```shell
kubectl port-forward --namespace ${NAMESPACE} ${APP_INSTANCE_NAME}-release 8080
```

With the port forwarded locally, you can access the Release UI with
`http://localhost:8080/`.

### Access Release externally

If you enabled the ingress parameter during the installation process, Release should be publicly available based on the other required parameters you provided. Otherwise, follow the steps below to expose the Release service.

To expose Release with a publicly available IP address, run the following
command:

```shell
kubectl patch svc "${APP_INSTANCE_NAME}-release-service" \
  --namespace "${NAMESPACE}" \
  -p '{"spec": {"type": "LoadBalancer"}}'
```

It might take a while for the service to be publicly available. After the
process is finished, get the public IP address with:

```shell
SERVICE_IP=$(kubectl get svc ${APP_INSTANCE_NAME}-release-service \
  --namespace ${NAMESPACE} \
  --output jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo "http://${SERVICE_IP}:8080/"
```

### Access the ArangoDB Dashboard

ArangoDB provides a web based dashboard to manage the database. To access the dashboard internally, follow the steps below:

You can use local port forwarding to acess the ArangoDB dashboard. In a background terminal, run the following command:

```shell
kubectl port-forward --namespace ${NAMESPACE} ${APP_INSTANCE_NAME}-arangodb 8529
```

With the port forwarded locally, you can access the dashboard with
`http://localhost:8529/`.

You can now login to ArangoDB using "root" as the username and the password you created earlier. This password is stored in a Kubernetes secret named "${APP_INSTANCE_NAME}-arangodb" should you need to reference it again.

# Scaling

This installation of Release is not intended to be scaled up.

# Backup and Restore

The following steps are based on the [arangodump documentation](https://docs.arangodb.com/3.11/components/tools/arangodump/).


# Upgrading the app

Before upgrading, we recommend that you prepare a backup of your ArangoDB
database, using the step above.

To update Release and its dependencies, run the commands below:

```shell
kubectl set image deployment/${APP_INSTANCE_NAME}-release -n=${NAMESPACE} release=[NEW_IMAGE_REFERENCE]
```

```shell
kubectl set image deployment/${APP_INSTANCE_NAME}-arangodb -n=${NAMESPACE} arangodb=[NEW_IMAGE_REFERENCE]
```

```shell
kubectl set image deployment/${APP_INSTANCE_NAME}-deployer -n=${NAMESPACE} deployer=[NEW_IMAGE_REFERENCE]
```

Where `[NEW_IMAGE_REFERENCE]` is the Docker image reference of the new image
that you want to use.

To check the status of Pods for each deployment, and the progress of the new
image, run the following command:

```shell
kubectl get pods --selector app.kubernetes.io/name=$APP_INSTANCE_NAME \
  --namespace ${NAMESPACE}
```

# Uninstall the Application

## Using the Google Cloud Platform Console

1. In the GCP Console, open [Kubernetes Applications](https://console.cloud.google.com/kubernetes/application).

1. From the list of applications, click **Release**.

1. On the Application Details page, click **Delete**.

## Using the command line

### Prepare the environment

Set your installation name and Kubernetes namespace:

```shell
export APP_INSTANCE_NAME=release
export NAMESPACE=default
```

### Delete the resources

> **NOTE:** We recommend using a `kubectl` version that is the same as the version of your cluster. Using the same versions of `kubectl` and the cluster helps avoid unforeseen issues.

To delete the resources, use the expanded manifest file used for the
installation.

Run `kubectl` on the expanded manifest file:

```shell
kubectl delete -f ${APP_INSTANCE_NAME}_manifest.yaml --namespace $NAMESPACE
```

Otherwise, delete the resources using types and a label:

```shell
kubectl delete application \
  --namespace $NAMESPACE \
  --selector app.kubernetes.io/name=$APP_INSTANCE_NAME
```

### Delete the GKE cluster

Optionally, if you don't need the deployed application or the GKE cluster,
delete the cluster using this command:

```shell
gcloud container clusters delete "$CLUSTER" --zone "$ZONE"
```