{{ define "render-value" }}
  {{- if kindIs "string" .value }}
    {{- tpl .value .context }}
  {{- else }}
    {{- tpl (.value | toYaml) .context }}     
  {{- end }}
{{- end }}

{{/*
Expand the name of the chart.
*/}}
{{- define "auditdeploy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "auditdeploy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create unified labels for auditdeploy components
*/}}
{{- define "auditdeploy.common.matchLabels" -}}
release: {{ .Release.Name }}
{{- end -}}

{{- define "auditdeploy.common.metaLabels" -}}
chart: {{ template "auditdeploy.chart" . }}
heritage: {{ .Release.Service }}
{{- end -}}

{{- define "auditdeploy.deployer.labels" -}}
{{ include "auditdeploy.deployer.matchLabels" . }}
{{ include "auditdeploy.common.metaLabels" . }}
{{- end -}}

{{- define "auditdeploy.deployer.matchLabels" -}}
app: {{ .Values.deployer.name }}
{{ include "auditdeploy.common.matchLabels" . }}
{{- end -}}

{{- define "auditdeploy.release.labels" -}}
{{ include "auditdeploy.release.matchLabels" . }}
{{ include "auditdeploy.common.metaLabels" . }}
{{- end -}}

{{- define "auditdeploy.release.matchLabels" -}}
app: {{ .Values.release.name }}
{{ include "auditdeploy.common.matchLabels" . }}
{{- end -}}

{{- define "auditdeploy.arangodb.labels" -}}
{{ include "auditdeploy.arangodb.matchLabels" . }}
{{ include "auditdeploy.common.metaLabels" . }}
{{- end -}}

{{- define "auditdeploy.arangodb.matchLabels" -}}
app: {{ .Values.arangodb.name }}
{{ include "auditdeploy.common.matchLabels" . }}
{{- end -}}

{{- define "auditdeploy.builder.labels" -}}
{{ include "auditdeploy.builder.matchLabels" . }}
{{ include "auditdeploy.common.metaLabels" . }}
{{- end -}}

{{- define "auditdeploy.builder.matchLabels" -}}
app: {{ .Values.builder.name }}
{{ include "auditdeploy.common.matchLabels" . }}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "auditdeploy.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified deployer name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "auditdeploy.deployer.fullname" -}}
{{- if .Values.deployer.fullnameOverride }}
{{- .Values.deployer.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.deployer.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.deployer.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified release name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "auditdeploy.release.fullname" -}}
{{- if .Values.release.fullnameOverride }}
{{- .Values.release.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.release.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.release.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified arangodb name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "auditdeploy.arangodb.fullname" -}}
{{- if .Values.arangodb.fullnameOverride }}
{{- .Values.arangodb.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.arangodb.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.arangodb.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified builder name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "auditdeploy.builder.fullname" -}}
{{- if .Values.builder.fullnameOverride }}
{{- .Values.builder.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.builder.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.builder.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "auditdeploy.labels" -}}
helm.sh/chart: {{ include "auditdeploy.chart" . }}
{{ include "auditdeploy.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "auditdeploy.selectorLabels" -}}
app.kubernetes.io/name: {{ include "auditdeploy.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use for the deployer component
*/}}
{{- define "auditdeploy.serviceAccountName.deployer" -}}
{{- if .Values.serviceAccounts.deployer.create -}}
    {{ default (include "auditdeploy.deployer.fullname" .) .Values.serviceAccounts.deployer.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccounts.deployer.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the service account to use for the release component
*/}}
{{- define "auditdeploy.serviceAccountName.release" -}}
{{- if .Values.serviceAccounts.release.create -}}
    {{ default (include "auditdeploy.release.fullname" .) .Values.serviceAccounts.release.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccounts.release.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the service account to use for the arangodb component
*/}}
{{- define "auditdeploy.serviceAccountName.arangodb" -}}
{{- if .Values.serviceAccounts.arangodb.create -}}
    {{ default (include "auditdeploy.arangodb.fullname" .) .Values.serviceAccounts.arangodb.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccounts.arangodb.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the service account to use for the builder component
*/}}
{{- define "auditdeploy.serviceAccountName.builder" -}}
{{- if .Values.serviceAccounts.builder.create -}}
    {{ default (include "auditdeploy.builder.fullname" .) .Values.serviceAccounts.builder.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccounts.builder.name }}
{{- end -}}
{{- end -}}

{{/*
Define the auditdeploy.namespace template if set with forceNamespace or .Release.Namespace is set
*/}}
{{- define "auditdeploy.namespace" -}}
{{- if .Values.forceNamespace -}}
{{ printf "namespace: %s" .Values.forceNamespace }}
{{- else -}}
{{ printf "namespace: %s" .Release.Namespace }}
{{- end -}}
{{- end -}}